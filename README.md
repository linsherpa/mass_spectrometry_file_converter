# Install cwltool packages: https://github.com/common-workflow-language/cwltool

Refer Image (msconvert_&_validation.png) for better understanding of the procedure



## Input 1 :

### Objective: To convert file using msconvert.

### Main file: msconvert_workflow_main.cwl
### supplementary file: msconvert_workflow_file.yml

### command: cwltool msconvert_workflow_main.cwl msconvert_workflow_file.yml

###output:
- [ ]  mzML converted File
- [ ] validatin file (yml)



## Input 2:

### Objective: To test the mzML file using FileInfo command from OpenMS

### Main File: validation.cwl
### supplementary file: output(yml) of Input 1 or any other files in same format.

### command: cwltool validation.cwl <output yml of Input1>

### output:
- [ ] test results in txt format.



## For test files , contact NFDI4Chem (TA4) members

