#!/usr/bin/env cwl-runner

cwlVersion: v1.0
class: CommandLineTool
label: Test of MSconvert output

baseCommand: ["FileInfo", "-v"]
hints:
  DockerRequirement:
    dockerPull: lincoln1010/pwiz-skyline-i-agree-to-vendor-licenses
    # dockerImageId: bee935ddfdac

inputs:
  in_file:
    type: File
    inputBinding:
      prefix: -in

outputs:
  output:
    type: File
    outputBinding:
      glob: $(inputs.in_file.nameroot)_validation_result.txt

stdout: $(inputs.in_file.nameroot)_validation_result.txt

