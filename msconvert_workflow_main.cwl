#!/usr/bin/env cwl-runner

cwlVersion: v1.0
class: Workflow
label: A workflow to convert RAW File to mzML

inputs:
# Verified for a single RAW file
  in_file:
    type: File

steps:
# step 1 converts the RAW File to mzML
  step1:
    run: msconvert.cwl
    in:
      in_file: in_file
    out: [outfile, output_string]

# Step2 to create a Validation-file  ready  for Validation using FileInfo(openMS)

  step2:
    run: validation_file_creation.cwl
    in:
      input: step1/output_string
    out: [output_file]

# For Validation
#  step3:
#    run: validation.cwl
#    in:
#      in_file: step2/output_file
#    out: [output]


outputs:
  output_file:
    type: File
    outputSource: step1/outfile

  output_file2:
    type: File
    outputSource: step2/output_file

#  output_file3:
#    type: File
#    outputSource: step3/output


s:author:
  - class: s:Person
    s:identifier: https://orcid.org/0000-0002-7899-7192
    s:email: mailto:sneumann@ipb-halle.de
    s:name: Steffen Neumann

s:citation: https://dx.doi.org/10.6084/m9.figshare.3115156.v2
s:codeRepository: https://github.com/common-workflow-language/common-workflow-language
s:dateCreated: "2021-6-14"
s:license: https://spdx.org/licenses/MIT

$namespaces:
  s: https://schema.org/
  edam: http://edamontology.org/
$schemas:
  - https://schema.org/version/latest/schemaorg-current-http.rdf
  - http://edamontology.org/EDAM_1.18.owl
